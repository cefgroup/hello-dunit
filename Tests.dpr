program Tests;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  TestFramework,
  TextTestRunner,
  TClasslib in 'TClasslib.pas';

var
  r : TTestResult;
begin
  WriteLn('Hello-DUnit Test Runner');
  r := TextTestRunner.RunRegisteredTests(rxbContinue);
  ExitCode := r.ErrorCount + r.FailureCount;
end.
