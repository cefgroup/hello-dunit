program Hello;

{$APPTYPE CONSOLE}

{%File 'buildspec.yml'}
{%File 'build.bat'}

uses
  SysUtils;

procedure Greet(greeting : PChar); stdcall; external 'Classlib.dll';

var g : String;
begin
  SetLength(g, 255);
  Greet(PChar(g));
  WriteLn(Trim(g));

  if (DebugHook <> 0) then
    ReadLn;
end.
