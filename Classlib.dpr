library Classlib;

procedure Greet(Buffer: PChar); stdcall;
var
  greeting : String;
begin
  greeting := 'Hi, DUnit!';
  Move(greeting[1], Buffer^, Length(greeting));
end;

exports
  Greet;

begin
end.
