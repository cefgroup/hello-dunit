unit TClasslib;

interface

uses
  StrUtils, SysUtils, TestFramework;

procedure Greet(greeting : PChar); stdcall; external 'Classlib.dll';

type
  TestClasslib = class(TTestCase)
    public
      procedure SetUp; override;
      procedure TearDown; override;
    published
      procedure TestGreeting;
  end;

implementation

procedure TestClasslib.SetUp;
begin
end;

procedure TestClasslib.TearDown;
begin
end;

// Greeting text should contain the word 'Hello'
procedure TestClasslib.TestGreeting;
var g : String;
begin
  SetLength(g, 255);
  Greet(PChar(g));
  Check(AnsiContainsText(LowerCase(Trim(g)), 'Hello'), 'Greeting should contain the word ''Hello''.');
end;

begin
  RegisterTest(TestClasslib.Suite);
end.
