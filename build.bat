ECHO Building "Hello DUnit" executables...

rd /S /Q .\bin
mkdir .\bin
dcc32.exe -B -Q Classlib -E".\bin"
dcc32.exe -B -Q Hello -E".\bin"
dcc32.exe -B -Q Tests -E".\bin"
.\bin\Tests
